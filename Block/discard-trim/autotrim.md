     autotrim=on|off
             When set to on space which has been recently freed, and is no
             longer allocated by the pool, will be periodically trimmed.  This
             allows block device vdevs which support BLKDISCARD, such as SSDs,
             or file vdevs on which the underlying file system supports hole-
             punching, to reclaim unused blocks.  The default setting for this
             property is off.

             Automatic TRIM does not immediately reclaim blocks after a free.
             Instead, it will optimistically delay allowing smaller ranges to
             be aggregated in to a few larger ones.  These can then be issued
             more efficiently to the storage.

             Be aware that automatic trimming of recently freed data blocks
             can put significant stress on the underlying storage devices.
             This will vary depending of how well the specific device handles
             these commands.  For lower end devices it is often possible to
             achieve most of the benefits of automatic trimming by running an
             on-demand 
- man zpool
