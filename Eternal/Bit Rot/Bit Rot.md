# Mdadm
https://unix.stackexchange.com/questions/105337/bit-rot-detection-and-correction-with-mdadm

# RedHat
https://www.redhat.com/en/blog/what-bit-rot-and-how-can-i-detect-it-rhel

# article:
https://www.jodybruchon.com/2017/03/07/zfs-wont-save-you-fancy-filesystem-fanatics-need-to-get-a-clue-about-bit-rot-and-raid-5/

# sch:
- https://www.google.com/search?q=lvm+raid+bit+rot
