# LVM
https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/configuring_and_managing_logical_volumes/index

# ZFS vs. *
https://www.klennet.com/notes/2019-07-04-raid5-vs-raidz.aspx

## `zpool import -F` after power loss
- https://github.com/openzfs/zfs/issues/4501
- https://www.ixsystems.com/community/threads/power-outage-effect-on-zfs.49143/


# ansible prime
- https://docs.ansible.com/ansible/latest/user_guide/playbooks_variables.html#defining-variables-in-files
- https://docs.ansible.com/ansible/latest/modules/parted_module.html
- https://docs.ansible.com/ansible/latest/modules/lvg_module.html
- https://docs.ansible.com/ansible/latest/modules/lvol_module.html

# automation
https://www.linuxsysadmins.com/creating-logical-volume-using-ansible/
