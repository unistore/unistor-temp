---
# Works!
- https://stackoverflow.com/questions/48036051/ansible-using-ansible-facts-how-can-i-get-a-disk-device-based-on-the-storage-c
- https://www.reddit.com/r/ansible/comments/b6hmee/help_get_the_unmounted_device_list/

# Best
- https://www.reddit.com/r/ansible/comments/b6hmee/help_get_the_unmounted_device_list/ek0hnbp/

```
- name: mount and create a new partion for houskeeping
  hosts: myhosts
  gather_facts: yes

  tasks:

    - name: get device name
      set_fact:
        device_name_full: "/dev/{{item}}"
        device_name: "{{item}}"
      when: ansible_facts.devices.{{item}}.partitions == {}
      with_items: "{{ ansible_facts.devices}}"
```

# from:
- https://www.google.com/search?q=ansible+list+of+hard+drives

